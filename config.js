//Define passwords and codes to be gitignored later
function config(){
    switch(process.env.NODE_ENV){
        default:
            return {
                'dbURI': 'mongodb://localhost/library',
                'port': 3050,
                SECRET: "secretjwt",
            };
    }
};

module.exports = config();
