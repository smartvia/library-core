const express = require('express');
const bodyParser = require('body-parser');
const config = require('./config');
const authentifier = require('./authentifier');

const app = express();
const API_PATH = '/api/'

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

//Start mongodb connection
const dbconnection = require('./database/db.js');

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
  next();
});
//TODO add logger
app.use(authentifier)

app.get('/api/', (req, res) => {
  res.send('I am working!')
})

const users = require('./routes/users')
app.use(API_PATH + 'users/', users);

const books = require('./routes/books')
app.use(API_PATH + 'books/', books);

// const boorrowings = require('./routes/borrowings')
// app.use(API_PATH + 'boorrowings/', boorrowings);
//

app.listen(config.port, () => console.log(`Listening on port ${config.port}`));
