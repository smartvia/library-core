const express  = require('express');
const router   = express.Router();
const ObjectId = require('mongodb').ObjectId;
const Book = require('../database/schemas/Book');
const isauth = require('../isauth');

// get all books
router.get('/', function(req, res, next) {
  let filter = {};

  let q = req.query.q
  if (q) {
    filter = {$text: {$search: q}}
  }

  Book
  .find(filter)
  .exec((err, books) => {
    if (err) {
      res.status(500).send({
        success: false,
        error: err
      });
    } else {
      res.status(200).send({
        success: true,
        data: books
      });
    }
  });
});

// get book
router.get('/:id', function(req, res, next) {

  const id = req.params.id;
  if (ObjectId.isValid(id)) {
    //   Document.findByIdAndRemove(req.params.id, function(err) {
    Book
    .findById(id)
    .exec((err, book) => {
      if (err) {
        res.status(500).send({
          success: false,
          error: err
        });
      } else {
        if (book) {
          res.status(200).send({
            success: true,
            data: book
          });
        } else {
          res.status(404).send({
            success: false,
            message: "No book found!"
          });
        }
      }
    });
  } else {
    res.status(500).send({
      success: false,
      message: "Incorect book ID!"
    });
  }

});

// DELETE Book
router.delete('/:id', isauth, function(req, res, next) {
  const id = req.params.id;
  if (ObjectId.isValid(id)) {
    //   Document.findByIdAndRemove(req.params.id, function(err) {
    Book
    .findByIdAndRemove(id)
    .exec((err, book) => {
      if (err) {
        res.status(500).send({
          success: false,
          error: err
        });
      } else {
        if (book) {
          res.status(200).send({
            success: true,
            message: "Book has been removed!"
          });
        } else {
          res.status(404).send({
            success: false,
            message: "No book found!"
          });
        }
      }
    });
  } else {
    res.status(500).send({
      success: false,
      message: "Incorect book ID!"
    });
  }
});


router.post('/', function(req, res, next) {
  var book = new Book();

  book.title = req.body.title;
  book.isbn = req.body.isbn;
  book.authors = req.body.authors;
  book.publishYear = req.body.publishYear;
  book.genres = req.body.genres;

  book.save(function(err) {
    if (err){
      res.status(500).send({
        success: false,
        error: err,
      });
    } else {
      res.status(200).send({
        success: true,
        data: {
          _id: book._id,
          title: book.title,
          isbn: book.isbn,
          authors: book.authors,
          publishYear: book.publishYear,
          genres: book.genres,
        }
      });
    }
  });
});

router.put('/:id', function(req, res, next) {
  const id = req.params.id;
  if (ObjectId.isValid(id)) {
    Book.findById(id, function(err, book) {
      if (err) {
        res.status(500).send({
          success: false,
          error: err,
        });
      } else {
        if (req.body.title)
          book.title = req.body.title;
        if (req.body.authors)
          book.authors = req.body.authors;
        if (req.body.isbn)
          book.isbn = req.body.isbn;
        if (req.body.genres)
          book.genres = req.body.genres;
        if (req.body.publishYear)
          book.publishYear = req.body.publishYear;


        book.save(function(err) {
          if (err) {
            res.status(500).send({
              success: false,
              error: err,
            });
          } else {
            res.status(200).send({
              success: true,
              message: "Book has been updated!"
            });
          }
        });

      }
    });
  } else {
    res.status(500).send({
      success: false,
      message: "Incorect book ID!"
    });
  }
});

module.exports = router;
