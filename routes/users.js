const express  = require('express');
const router   = express.Router();
const config = require('../config');
const ObjectId = require('mongodb').ObjectId;

const User = require('../database/schemas/User');
const PRIVACY_FILTER = {password: 0, email: 0}
const isauth = require('../isauth');


const jwt = require('jsonwebtoken');

// GET TOKEN
// create valid token for valid user
router.post('/token', function(req, res, next) {
  var user = new User();

  const password = req.body.password;
  const email = req.body.email;

  if (password && email) {
    User
    .findOne({email: email})
    .exec((err, user) => {
      if (err) {
        res.status(500).send({
          success: false,
          error: err,
        });
      } else {
        if (user) {
          user.comparePassword(password, function(err, isMatch){
            if(isMatch){
              const tokenContent = {
                _id: user._id,
                name: user.name,
                roles: user.roles,
              };
              const token = jwt.sign(tokenContent, config.SECRET, {
                expiresIn: '30d' // expires in 30 days
              });

              res.status(200).json({
                success: true,
                token: token
              });
            } else {
              res.status(401).json({
                success: false,
                message: "Wrong Login or Password"
              });
            }
          });
        } else {
          res.status(401).json({
            success: false,
            message: "Wrong Login or Password"
          });
        }
      }
    });
  } else {
    res.status(500).send({
      success: false,
      message: "Password or email not provided!"
    });
  }
});

// GET ALL USERS
router.get('/', isauth, function(req, res, next) {
  User
  .find({}, PRIVACY_FILTER)
  .exec((err, users) => {
    if (err) {
      res.status(500).send({
        success: false,
        error: err
      });
    } else {
      res.status(200).send({
        success: true,
        data: users
      });
    }
  });
});

// GET User
router.get('/:id', isauth, function(req, res, next) {
  const id = req.params.id;
  if (ObjectId.isValid(id)) {
    User
    .findOne({_id: id}, PRIVACY_FILTER)
    .exec((err, user) => {
      if (err) {
        res.status(500).send({
          success: false,
          error: err
        });
      } else {
        if (user) {
          res.status(200).send({
            success: true,
            data: user
          });
        } else {
          res.status(404).send({
            success: false,
            message: "No user found!"
          });
        }
      }
    });
  } else {
    res.status(500).send({
      success: false,
      message: "Incorect user ID!"
    });
  }
});


module.exports = router;
