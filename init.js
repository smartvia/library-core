const fs = require('fs');
const mongoose = require('mongoose');
const config = require('./config');

//Start mongodb connection
const dbconnection = require('./database/db.js');

const Book = require('./database/schemas/Book');
const Borrowing = require('./database/schemas/Borrowing');
const User = require('./database/schemas/User');

function createUser(email, password,  name, roles, _id) {
  const user = new User();

  if (_id) {
    user._id = _id;
  }

  user.email = email;
  user.password = password;
  user.name = name;
  user.roles = roles;

  return(user);
}

function createBook(title, isbn, authors, publishYear, genres, _id) {
  const book = new Book();
  if (_id) {
    book._id = _id;
  }

  book.title = title;
  book.isbn = isbn;
  book.authors = authors;
  book.publishYear = publishYear;
  book.genres = genres;

  return(book);
}

function createBorrowing(user, book, startDate, endDate, note, _id) {
  const borrowing = new Borrowing();
  if (_id) {
    borrowing._id = _id;
  }

  borrowing.user = user;
  borrowing.book = book;
  borrowing.startDate = startDate;
  borrowing.endDate = endDate;
  borrowing.note = note;

  return(borrowing);
}


async function main(application) {
  console.log("PROCESSING ...");

  await Borrowing.deleteMany({});
  await Book.deleteMany({});
  await User.deleteMany({});


  const admin = createUser('admin@admin.sk', 'peter', 'Peter Admin', 'Admin');
  await admin.save();

  const student1 = createUser('ivan@student.sk', 'ivan', 'Ivan Student', 'Student');
  await student1.save();

  const student2 = createUser('juraj@student.sk', 'juraj', 'Juraj Student', 'Student');
  await student2.save();

  const book1 = createBook("The World According To Garp", "9781474614405", ["John Irving"], 2019, ["fiction"]);
  await book1.save();

  const book2 = createBook("A Prayer for Owen Meany: A Novel", "9780062204226", ["John Irving"], 2013, ["fiction"]);
  await book2.save();

  const book3 = createBook("The Cider House Rules", "0552146137", ["John Irving"], 1999, ["fiction"]);
  await book3.save();

  const book4 = createBook("Color and Light: A Guide for the Realist Painter", "0740797719", ["James Gurney "], 2010, ["non fiction"]);
  await book4.save();

  const book5 = createBook("1984", "9788076420908", ["George Orwell,","Matyas Namai"], 2021, ["graphic novel", "sci-fi"]);
  await book5.save();

  const borrowing1 = createBorrowing(student1, book1, new Date(), null, "Thank you! It was great book.");
  await borrowing1.save();

  const borrowing2 = createBorrowing(student1, book2, new Date(), new Date(), "Thank you! I don't like it.");
  await borrowing2.save();
  
  await dbconnection.disconnectDB();
  process.exit();
}

var stdin = process.openStdin();

// Wait while db is initialized
setTimeout(() => {
  console.log("==================================================================");
  console.log("ARE YOU SURE, ALL DATA IN " + config.dbURI + " WILL BE DELETED (Y/N)!: [N]");
}, 2000);

stdin.addListener("data", function(d) {
    if (d.toString().trim() == "Y") {
      main();
    } else {
      process.exit();
    }
  });
