const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const BorrowingSchema = new Schema({
  startDate: {
    type: Date
  },
  endDate: {
    type: Date
  },
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  book: {
    type: Schema.Types.ObjectId,
    ref: 'Book'
  },
  note: {
    type: String
  },
},
{
  timestamps: true
});

module.exports = mongoose.model('Borrowing', BorrowingSchema);
