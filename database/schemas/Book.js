const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const BookSchema = new Schema({
  title: {
    type: String
  },
  isbn: {
    type: String
  },
  authors: [{
    type: String
  }],
  publishYear: {
    type: Number
  },
  genres: [{
    type: String
  }]
},
{
  timestamps: true
});

BookSchema.index({title: 'text', 'authors': 'text', 'genres': 'text'});

module.exports = mongoose.model('Book', BookSchema);
