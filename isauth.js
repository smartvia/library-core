// TODO check role isroot, isstudent, ...

function isauth(req, res, next) {
  if(req.user) {
    next()
  } else {
    res.status(401).json({
      success: false,
      message: "Unauthorized access to resource"
    })
  }
};

module.exports = isauth;
